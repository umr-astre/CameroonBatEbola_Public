/* file model.c */
#include <stdio.h> /* For printf */
#include <R.h>
#include <math.h> /* double fmax( double x, double y ); float fmaxf( float x, float y ); long double fmaxl( long double x , long double y ); */

/* Global Constants */
#define nParams     18
#define nStates     20
#define weeksInYear 52.0
#define epsillon  1E-2 /* Helps avoid artifact (spike) due to truncation by modulo opperator & rounding error  */
#define buffer     2.0 /* Gap in weeks between end of a pulse and start of preceeding pulse */
#define gamma     10.0 /* Shape parameter of pulse function */

static double parameters[nParams];
double *state;
double repeatedValue[nParams];

/* Globally defined parameters. See vignette("compiledCode")  */
#define bStart			parameters[0]
#define bDuration		parameters[1]
#define pBirthMature    parameters[2]
#define wStart  		parameters[3]
#define jmStart 		parameters[4]
#define imStart 		parameters[5]
#define maxRateWean		parameters[6]
#define maxRateJuvMat	parameters[7]
#define maxRateImmMat	parameters[8]
#define mu1				parameters[9]
#define mu2				parameters[10]
#define K				parameters[11]
#define beta			parameters[12]
#define rateRecovery	parameters[13]
#define rateMatAbLoss	parameters[14]
#define rateAbLoss		parameters[15]
#define pR2L			parameters[16]
#define pL2R			parameters[17]

double halfBirthRate;
double birthRate;
double weanRate;
double juvMatRate;
double immMatRate;
double birthRateMax;
double weanRateMax;
double juvMatRateMax;
double immMatRateMax;
double allBigBats;
double mu2DD;
double mu1DD;
double pR2S;
double allInfectious;
double mu1DD_plus_weanRate;
double mu1DD_plus_juvMatRate;
double mu1DD_plus_immMatRate;
double beta_x_allI;
double beta_x_pL2R;
double beta_x_pL2R_x_allI;
double pBirthMax;

double bStop;
double wStop;
double jmStop;
double imStop;
double wDuration;
double jmDuration;
double imDuration;
double duration;
double shift;
double X1;
double X2;


double M_Pup;
double S_Pup;
double I_Pup;
double R_Pup;
double L_Pup;
double M_Juv;
double S_Juv;
double I_Juv;
double R_Juv;
double L_Juv;
double M_Imm;
double S_Imm;
double I_Imm;
double R_Imm;
double L_Imm;
double S_Ad;
double I_Ad;
double R_Ad;
double L_Ad;
double Surv;

int ii;
double xyz;


/* Indices for each state */
#define iM_Pup 0
#define iS_Pup 1
#define iI_Pup 2
#define iR_Pup 3
#define iL_Pup 4
#define iM_Juv 5
#define iS_Juv 6
#define iI_Juv 7
#define iR_Juv 8
#define iL_Juv 9
#define iM_Imm 10
#define iS_Imm 11
#define iI_Imm 12
#define iR_Imm 13
#define iL_Imm 14
#define iS_Ad  15
#define iI_Ad  16
#define iR_Ad  17
#define iL_Ad  18
#define iSurv  19

double rMod(double a, double b) {
	/* fmod doesn't handle -ves the same way as R's %%, so here's a solution */
	double c;
	if( a < 0 ) {
		c = b - fmod(-1.0 * a, b);
	} else {
		c = fmod(a, b);
    }
	return c;
}


double pulseFunction(double x, double tStart, double tStop, double rateMax) {
    /* x      - a time variable (scalar or vector)        */
    /* tStart - inflection point defining start of pulse  */
	/* tStop  - inflection point defining end of pulse    */
    duration = rMod(tStop - tStart, weeksInYear);
    /* Apply a shift to minimise effects of truncation by the modulo operator */
	shift = (weeksInYear + duration - 1e-2) / 2.0; /* The 1e-2 helps avoid a rounding era artifact (spike) */
    X1    = rMod((tStart - x + shift), weeksInYear) - shift;
	X2    = rMod((x  - tStop + shift), weeksInYear) - shift;
	/* Return rate at time x */
    double out = rateMax * 1.0/(1.0+exp(gamma * X1)) * 1.0/(1.0+exp(gamma * X2));
	/* printf("x=%e, tStart=%e, tStop=%e, duration=%e, shift=%e, X1=%e, X2=%e, out=%e\n", x, tStart, tStop, duration, shift, X1, X2, out); */
	return out;
}

void pulseWrapper(double *x, double *tStart, double *tStop, double *rateMax, double *out) {
	/* printf("TEST"); */
	/* printf("Value of out = %e\n", *out); */
	*out = pulseFunction(*x, *tStart, *tStop, *rateMax);
	/* printf("Value of out = %e\n", *out); */
	/* return  *out; */
}


/* double birthRateFunction(double x) { */
/*     /\* x         - a time variable (scalar or vector). *\/ */
/*     /\* bStart    - start of birth pulse. *\/ */
/*     /\* bDuration - length of birth pulse. *\/ */
/*     /\* pBirthMax    - proportion of females contributing to birth pulse. Assumes one pup each. *\/ */

/*     /\* This shift helps maintain continuity at zero when bStart is close to zero *\/ */
/* 	bStop         = bStart + bDuration; */
/*     pBirthMax		  = pBirthMature * approxYearSurvivalAd; /\* The proportion that survive the first year of adulthood can go on to reproduce *\/ */
/*     birthRateMax  = - log(1.0 - pBirthMax) / bDuration; */

/* 	double out = pulseFunction(*t, bStart, bStop, birthRateMax); */
/* 	/\* printf("Value of out = %e\n", out); *\/ */
/* 	return out; */
/* } */



/* Initializer for ODE solver */
void initmod(void (* odeparms)(int *, double *)) {
	int N=nParams;
	odeparms(&N, parameters);
}

/* Initializer for testing functions in R */
void initR(double *paras) {
	int N=nParams;
	for (ii = 0; ii < N; ii++) {
		parameters[ii] = paras[ii];
	}
}


/* Derivatives and 1 output variable */
void derivs (int    *neq,  /* Number of equations */
			 double *t,    /* Value of independent variable (time) */
			 double *y,    /* Points to double precision array of length *neq containing current state */
			 double *ydot,
			 double *yout, /* double precision vector whose ﬁrst nout values are other output variables (difference from the state variables y), and the next values are double precision values as passed by parameter rpar when calling the integrator. The key to the elements of *yout is set in *ip */
			 int    *ip    /* points to an integer vector whose length is at least 3; the ﬁrst element (ip[0]) contains the number of output values (which should be equal or larger than nout), its second element contains the length of *yout, and the third element contains the length of *ip; next are integer values, as passed by parameter ipar when calling the integrator. */
			 ) {
	if (ip[0] < 1) error("nout should be at least 1");

    state = y;

	/* New in v1.0.9: Force positivity to counter machine imprecission near zero */
    for (ii=0; ii<nStates; ii++) {
		xyz = fmax(state[ii],0.0);
		state[ii] = xyz;
	}

	/* States */
	M_Pup  = state[iM_Pup];
	S_Pup  = state[iS_Pup];
	I_Pup  = state[iI_Pup];
	R_Pup  = state[iR_Pup];
	L_Pup  = state[iL_Pup];
	M_Juv  = state[iM_Juv];
	S_Juv  = state[iS_Juv];
	I_Juv  = state[iI_Juv];
	R_Juv  = state[iR_Juv];
	L_Juv  = state[iL_Juv];
	M_Imm  = state[iM_Imm];
	S_Imm  = state[iS_Imm];
	I_Imm  = state[iI_Imm];
	R_Imm  = state[iR_Imm];
	L_Imm  = state[iL_Imm];
	S_Ad   = state[iS_Ad];
	I_Ad   = state[iI_Ad];
	R_Ad   = state[iR_Ad];
	L_Ad   = state[iL_Ad];
	Surv   = state[iSurv];

	/* Derived variables */
	/* imStart             = bStart + weeksInYear ; */
	bStop               = bStart + bDuration;
	wStop               = rMod(bStart	- buffer, weeksInYear);
    jmStop              = rMod(wStart	- buffer, weeksInYear);
    imStop              = rMod(jmStart	- buffer, weeksInYear);
    wDuration           = rMod(wStop	- wStart, weeksInYear);
    jmDuration          = rMod(jmStop	- jmStart, weeksInYear);
    imDuration          = rMod(imStop	- imStart, weeksInYear);
	birthRateMax		= pBirthMature / bDuration;  /* * approxYearSurvivalAd ; */
	/* The proportion of matures giving birth per year / length of birth period * proportion that survived first year as adult */

	birthRate			= pulseFunction(*t, bStart,  bStop,  birthRateMax);
	weanRate			= pulseFunction(*t, wStart,  wStop,  maxRateWean);
	juvMatRate			= pulseFunction(*t, jmStart, jmStop, maxRateJuvMat);
	immMatRate			= pulseFunction(*t, imStart, imStop, maxRateImmMat);

	allBigBats			= M_Juv + S_Juv + I_Juv + R_Juv + L_Juv + M_Imm + S_Imm + I_Imm + R_Imm + L_Imm + S_Ad + I_Ad + R_Ad + L_Ad;
	mu2DD				= mu2 * (1.0 + allBigBats / K);
	mu1DD				= mu1 + mu2DD;
	pR2S				= 1.0 - pR2L;
	allInfectious		= I_Pup + I_Juv + I_Imm + I_Ad;
	halfBirthRate		= birthRate * 0.5;
	beta_x_allI			= beta * allInfectious;
	beta_x_pL2R_x_allI	= beta_x_allI * pL2R;

	/* Just to go a little faster */
	mu1DD_plus_weanRate   = mu1DD + weanRate;
	mu1DD_plus_juvMatRate = mu1DD + juvMatRate;
	mu1DD_plus_immMatRate = mu1DD + immMatRate;

	/* printf("Value of *t = %f\n", *t); */
	/* printf("Value of bStart = %f\n", bStart); */
	/* printf("Value of bDuration = %f\n", bDuration); */
	/* printf("Value of pBirthMax = %f\n", pBirthMax); */
	/* printf("Value of state[0] = %f\n", state[0]); */
	/* printf("Value of halfBirthRate = %f\n", halfBirthRate); */
    /* printf("Value of birthRate		= %f\n", birthRate); */
    /* printf("Value of weanRate		= %f\n", weanRate); */
    /* printf("Value of juvMatRate		= %f\n", juvMatRate); */
    /* printf("Value of immMatRate		= %f\n", immMatRate); */
	/* printf("Value of bStart			= %f\n", bStart); */
	/* printf("Value of bStop			= %f\n", bStop); */
	/* printf("Value of birthRateMax	= %f\n", birthRateMax); */
	/* printf("Value of wStart  		= %f\n", wStart); */
	/* printf("Value of wStop			= %f\n", wStop); */
	/* printf("Value of maxRateWean	= %f\n", maxRateWean); */
	/* printf("Value of jmStart		= %f\n", jmStart); */
	/* printf("Value of jmStop			= %f\n", jmStop); */
	/* printf("Value of maxRateJuvMat	= %f\n", maxRateJuvMat); */
	/* printf("Value of imStart		= %f\n",imStart); */
	/* printf("Value of imStop			= %f\n",imStop); */
	/* printf("Value of maxRateImmMat	= %f\n",maxRateImmMat); */

    /* printf("Value of R_Ad = %f\n", R_Ad); */
	/* printf("Value of L_Ad = %f\n", L_Ad); */
    /* printf("Value of M_Pup = %f\n", M_Pup); */
    /* printf("Value of mu1DD = %f\n", mu1DD); */
    /* printf("Value of rateMatAbLoss = %f\n", rateMatAbLoss); */
    /* printf("Value of wStart = %f\n", bStart + weanLag); */
    /* printf("Value of wStop = %f\n",  bStart - 2 ); */
    /* printf("Value of pWean = %f\n",  pWean); */
    /* printf("Value of bDuration = %f\n", bDuration); */
	/* printf("Value of mu1DD_plus_weanRate = %f\n", mu1DD_plus_weanRate); */
    /* printf("Value of rateMatAbLoss = %f\n", rateMatAbLoss); */

	/* ODE system */
	ydot[iM_Pup] = halfBirthRate * R_Ad                                                                  - M_Pup * (mu1DD_plus_weanRate + rateMatAbLoss);
	ydot[iS_Pup] = halfBirthRate * (S_Ad+I_Ad+L_Ad) + rateMatAbLoss * M_Pup + rateAbLoss * R_Pup * pR2S  - S_Pup * (mu1DD_plus_weanRate + beta_x_allI);
	ydot[iI_Pup] = S_Pup * beta_x_allI                                                                   - I_Pup * (mu1DD_plus_weanRate + rateRecovery);
 	ydot[iR_Pup] = rateRecovery * I_Pup	+ beta_x_pL2R_x_allI * L_Pup                                     - R_Pup * (mu1DD_plus_weanRate + rateAbLoss);            /* Update v1.0.9 */
	ydot[iL_Pup] = rateAbLoss * R_Pup * pR2L                                                             - L_Pup * (mu1DD_plus_weanRate + beta_x_pL2R_x_allI);    /* Update v1.0.9 */
	ydot[iM_Juv] = weanRate * M_Pup																- M_Juv * (mu1DD_plus_juvMatRate + rateMatAbLoss);
	ydot[iS_Juv] = weanRate * S_Pup + rateMatAbLoss * M_Juv + rateAbLoss * R_Juv * pR2S         - S_Juv * (mu1DD_plus_juvMatRate + beta_x_allI);
	ydot[iI_Juv] = weanRate * I_Pup + S_Juv * beta_x_allI                                       - I_Juv * (mu1DD_plus_juvMatRate + rateRecovery);
	ydot[iR_Juv] = weanRate * R_Pup + rateRecovery * I_Juv + beta_x_pL2R_x_allI * L_Juv         - R_Juv * (mu1DD_plus_juvMatRate + rateAbLoss);          /* Update v1.0.9 */
	ydot[iL_Juv] = weanRate * L_Pup + rateAbLoss * R_Juv * pR2L									- L_Juv * (mu1DD_plus_juvMatRate  + beta_x_pL2R_x_allI); /* Update v1.0.9 */
	ydot[iM_Imm] = juvMatRate * M_Juv															- M_Imm * (mu1DD_plus_immMatRate + rateMatAbLoss);
	ydot[iS_Imm] = juvMatRate * S_Juv + rateMatAbLoss * M_Imm + rateAbLoss * R_Imm * pR2S       - S_Imm * (mu1DD_plus_immMatRate + beta_x_allI);
	ydot[iI_Imm] = juvMatRate * I_Juv + S_Imm * beta_x_allI                                     - I_Imm * (mu1DD_plus_immMatRate + rateRecovery);
	ydot[iR_Imm] = juvMatRate * R_Juv + rateRecovery * I_Imm + beta_x_pL2R_x_allI * L_Imm       - R_Imm * (mu1DD_plus_immMatRate + rateAbLoss);          /* Update v1.0.9 */
	ydot[iL_Imm] = juvMatRate * L_Juv + rateAbLoss * R_Imm * pR2L								- L_Imm * (mu1DD_plus_immMatRate + beta_x_pL2R_x_allI);  /* Update v1.0.9 */
	ydot[iS_Ad]  = immMatRate * (S_Imm + M_Imm) + rateAbLoss * R_Ad * pR2S						- S_Ad * (mu2DD + beta_x_allI);
	ydot[iI_Ad]  = immMatRate * I_Imm + S_Ad * beta_x_allI                                      - I_Ad * (mu2DD + rateRecovery);
	ydot[iR_Ad]  = immMatRate * R_Imm + rateRecovery * I_Ad + beta_x_pL2R_x_allI * L_Ad         - R_Ad * (mu2DD + rateAbLoss);                           /* Update v1.0.9 */
	ydot[iL_Ad]  = immMatRate * L_Imm + rateAbLoss * R_Ad * pR2L                                - L_Ad * (mu2DD + beta_x_pL2R_x_allI);                   /* Update v1.0.9 */
	ydot[iSurv]  = -Surv * mu2DD;

	/* Required for compatibility, but is it useful? */
	/* yout[0] = M_Pup +  S_Pup +  I_Pup +  R_Pup +  L_Pup +  M_Juv +  S_Juv +  I_Juv +  R_Juv +  L_Juv +  M_Imm +  S_Imm +  I_Imm +  R_Imm +  L_Imm +  S_Ad +  I_Ad +  R_Ad +  L_Ad; */
}



void jac(int    *neq,
		 double *t,
		 double *y,
		 int    *ml,
		 int    *mu,
		 double *pd,
		 int    *nrowpd,
		 double *yout,
		 int    *ip) {

	state = y;

    /* New in v1.0.9: Force positivity to counter machine imprecission near zero */
    for (ii=0; ii<nStates; ii++) {
		xyz = fmax(state[ii],0.0);
		state[ii] = xyz;
	}

	/* States */
	M_Pup  = state[iM_Pup];
	S_Pup  = state[iS_Pup];
	I_Pup  = state[iI_Pup];
	R_Pup  = state[iR_Pup];
	L_Pup  = state[iL_Pup];
	M_Juv  = state[iM_Juv];
	S_Juv  = state[iS_Juv];
	I_Juv  = state[iI_Juv];
	R_Juv  = state[iR_Juv];
	L_Juv  = state[iL_Juv];
	M_Imm  = state[iM_Imm];
	S_Imm  = state[iS_Imm];
	I_Imm  = state[iI_Imm];
	R_Imm  = state[iR_Imm];
	L_Imm  = state[iL_Imm];
	S_Ad   = state[iS_Ad];
	I_Ad   = state[iI_Ad];
	R_Ad   = state[iR_Ad];
	L_Ad   = state[iL_Ad];
	Surv   = state[iSurv];

	/* Derived variables */
	/* imStart             = bStart + weeksInYear; */
	bStop               = bStart + bDuration;
	wStop               = rMod(bStart - buffer, weeksInYear);
    jmStop              = rMod(wStart - buffer, weeksInYear);
    imStop              = rMod(jmStart - buffer, weeksInYear);
    wDuration           = rMod(wStop  - wStart, weeksInYear);
    jmDuration          = rMod(jmStop - jmStart, weeksInYear);
    imDuration          = rMod(imStop - imStart, weeksInYear);
	birthRateMax 	    = pBirthMature / bDuration; /* * approxYearSurvivalAd; */ /* The proportion that survive the first year of adulthood can go on to reproduce */

	birthRate			= pulseFunction(*t, bStart, bStop, birthRateMax);
	weanRate			= pulseFunction(*t, wStart, wStop, maxRateWean);
	juvMatRate			= pulseFunction(*t, jmStart, jmStop, maxRateJuvMat);
	immMatRate			= pulseFunction(*t, imStart, imStop, maxRateImmMat);
	allBigBats			= M_Juv + S_Juv + I_Juv + R_Juv + L_Juv + M_Imm + S_Imm + I_Imm + R_Imm + L_Imm + S_Ad + I_Ad + R_Ad + L_Ad;
	mu2DD				= mu2 * (1 + allBigBats / K);
	mu1DD				= mu1 + mu2DD;
	pR2S				= 1 - pR2L;
	allInfectious		= I_Pup + I_Juv + I_Imm + I_Ad;
    halfBirthRate		= birthRate * 0.5;
	beta_x_allI			= beta * allInfectious;
	beta_x_pL2R_x_allI	= beta_x_allI * pL2R;

	/* Just to go a little faster */
	mu1DD_plus_weanRate   = mu1DD + weanRate;
	mu1DD_plus_juvMatRate = mu1DD + juvMatRate;
	mu1DD_plus_immMatRate = mu1DD + immMatRate;

    /* Partial derivatives - column 1 */
	/* ydot[iM_Pup] = halfBirthRate * R_Ad - M_Pup * (mu1DD_plus_weanRate + rateMatAbLoss) */
	repeatedValue[iM_Pup]         = - M_Pup * mu2 / K;
	pd[iM_Pup * nStates + iM_Pup] = - (mu1DD_plus_weanRate + rateMatAbLoss);
	pd[iM_Pup * nStates + iM_Juv] = repeatedValue[iM_Pup];
	pd[iM_Pup * nStates + iS_Juv] = repeatedValue[iM_Pup];
	pd[iM_Pup * nStates + iI_Juv] = repeatedValue[iM_Pup];
	pd[iM_Pup * nStates + iR_Juv] = repeatedValue[iM_Pup];
	pd[iM_Pup * nStates + iL_Juv] = repeatedValue[iM_Pup];
	pd[iM_Pup * nStates + iM_Imm] = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iS_Imm] = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iI_Imm] = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iR_Imm] = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iL_Imm] = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iS_Ad]  = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iI_Ad]  = repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iR_Ad]  = halfBirthRate + repeatedValue[iM_Pup];
    pd[iM_Pup * nStates + iL_Ad]  = repeatedValue[iM_Pup];
    /* Partial derivatives - column 2 */
	/* ydot[iS_Pup] = halfBirthRate * (S_Ad+I_Ad+L_Ad) + rateMatAbLoss * M_Pup + rateAbLoss * R_Pup * pR2S - S_Pup * (mu1DD_plus_weanRate + beta_x_allI); */
	repeatedValue[iS_Pup]         = - S_Pup * mu2 / K;
    pd[iS_Pup * nStates + iM_Pup] = rateMatAbLoss;
	pd[iS_Pup * nStates + iS_Pup] = - (mu1DD_plus_weanRate + beta_x_allI);
	pd[iS_Pup * nStates + iI_Pup] = - S_Pup * beta;
	pd[iS_Pup * nStates + iR_Pup] = rateAbLoss * pR2S;
	pd[iS_Pup * nStates + iM_Juv] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iS_Juv] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iI_Juv] = pd[iS_Pup * nStates + iI_Pup] + repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iR_Juv] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iL_Juv] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iM_Imm] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iS_Imm] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iI_Imm] = pd[iS_Pup * nStates + iI_Juv];
	pd[iS_Pup * nStates + iR_Imm] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iL_Imm] = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iS_Ad]  = halfBirthRate + repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iI_Ad]  = halfBirthRate + pd[iS_Pup * nStates + iI_Juv];
	pd[iS_Pup * nStates + iR_Ad]  = repeatedValue[iS_Pup];
	pd[iS_Pup * nStates + iL_Ad]  = halfBirthRate + repeatedValue[iS_Pup];
    /* Partial derivatives - column 3 */
    /* ydot[iI_Pup] = S_Pup * beta_x_allI - I_Pup * (mu1DD_plus_weanRate + rateRecovery) */
	repeatedValue[iI_Pup]         = - I_Pup * mu2 / K;
    pd[iI_Pup * nStates + iS_Pup] = beta_x_allI;
	pd[iI_Pup * nStates + iI_Pup] = beta * S_Pup - (mu1DD_plus_weanRate + rateRecovery);
    pd[iI_Pup * nStates + iM_Juv] = repeatedValue[iI_Pup];
	pd[iI_Pup * nStates + iS_Juv] = repeatedValue[iI_Pup];
	pd[iI_Pup * nStates + iI_Juv] = beta * S_Pup + repeatedValue[iI_Pup];
	pd[iI_Pup * nStates + iR_Juv] = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iL_Juv] = repeatedValue[iI_Pup];
	pd[iI_Pup * nStates + iM_Imm] = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iS_Imm] = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iI_Imm] = pd[iI_Pup * nStates + iI_Juv];
    pd[iI_Pup * nStates + iR_Imm] = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iL_Imm] = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iS_Ad]  = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iI_Ad]  = pd[iI_Pup * nStates + iI_Juv];
    pd[iI_Pup * nStates + iR_Ad]  = repeatedValue[iI_Pup];
    pd[iI_Pup * nStates + iL_Ad]  = repeatedValue[iI_Pup];
    /* Partial derivatives - column 4 */
    /* ydot[iR_Pup] = rateRecovery * I_Pup	+ beta_x_pL2R_x_allI * L_Pup - R_Pup * (mu1DD_plus_weanRate + rateAbLoss) */
	repeatedValue[iR_Pup] = - R_Pup * mu2 / K;
    pd[iR_Pup * nStates + iI_Pup] = rateRecovery + beta_x_pL2R * L_Pup ;
	pd[iR_Pup * nStates + iR_Pup] = - (mu1DD_plus_weanRate + rateAbLoss);
	pd[iR_Pup * nStates + iL_Pup] = beta_x_pL2R_x_allI;
	pd[iR_Pup * nStates + iM_Juv] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iS_Juv] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iI_Juv] = repeatedValue[iR_Pup] + beta_x_pL2R * L_Pup;
    pd[iR_Pup * nStates + iR_Juv] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iL_Juv] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iM_Imm] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iS_Imm] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iI_Imm] = repeatedValue[iR_Pup] + beta_x_pL2R * L_Pup;
    pd[iR_Pup * nStates + iR_Imm] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iL_Imm] = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iS_Ad]  = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iI_Ad]  = repeatedValue[iR_Pup] + beta_x_pL2R * L_Pup;
    pd[iR_Pup * nStates + iR_Ad]  = repeatedValue[iR_Pup];
    pd[iR_Pup * nStates + iL_Ad]  = repeatedValue[iR_Pup];
    /* Partial derivatives - column 5 */
    /* ydot[iL_Pup] = rateAbLoss * R_Pup * pR2L - L_Pup * (mu1DD_plus_weanRate + beta_x_pL2R_x_allI) */
	repeatedValue[iL_Pup]         = - L_Pup * mu2 / K;
	pd[iL_Pup * nStates + iI_Pup] = - L_Pup * beta_x_pL2R;
    pd[iL_Pup * nStates + iR_Pup] = rateAbLoss * pR2L;
	pd[iL_Pup * nStates + iL_Pup] = - (mu1DD_plus_weanRate + beta_x_pL2R_x_allI);
    pd[iL_Pup * nStates + iM_Juv] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iS_Juv] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iI_Juv] = repeatedValue[iL_Pup] - L_Pup * beta_x_pL2R;
    pd[iL_Pup * nStates + iR_Juv] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iL_Juv] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iM_Imm] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iS_Imm] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iI_Imm] = repeatedValue[iL_Pup] - L_Pup * beta_x_pL2R;
    pd[iL_Pup * nStates + iR_Imm] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iL_Imm] = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iS_Ad]  = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iI_Ad]  = repeatedValue[iL_Pup] - L_Pup * beta_x_pL2R;
    pd[iL_Pup * nStates + iR_Ad]  = repeatedValue[iL_Pup];
    pd[iL_Pup * nStates + iL_Ad]  = repeatedValue[iL_Pup];
    /* Partial derivatives - column 6 */
    /* ydot[iM_Juv] = weanRate * M_Pup - M_Juv * (mu1DD_plus_juvMatRate + rateMatAbLoss) */
	repeatedValue[iM_Juv] = - M_Juv * mu2 / K;
	pd[iM_Juv * nStates + iM_Pup] = weanRate;
    pd[iM_Juv * nStates + iS_Juv] = repeatedValue[iM_Juv];
	pd[iM_Juv * nStates + iM_Juv] = - (mu1DD_plus_juvMatRate + rateMatAbLoss) + repeatedValue[iM_Juv];
	pd[iM_Juv * nStates + iI_Juv] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iR_Juv] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iL_Juv] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iM_Imm] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iS_Imm] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iI_Imm] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iR_Imm] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iL_Imm] = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iS_Ad]  = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iI_Ad]  = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iR_Ad]  = repeatedValue[iM_Juv];
    pd[iM_Juv * nStates + iL_Ad]  = repeatedValue[iM_Juv];
    /* Partial derivatives - column 7 */
    /* ydot[iS_Juv] = weanRate * S_Pup + rateMatAbLoss * M_Juv + rateAbLoss * R_Juv * pR2S - S_Juv * (mu1DD_plus_juvMatRate + beta_x_allI) */
	repeatedValue[iS_Juv]         = - S_Juv * mu2 / K;
	pd[iS_Juv * nStates + iS_Pup] = weanRate;
	pd[iS_Juv * nStates + iI_Pup] = - S_Juv * beta;
	pd[iS_Juv * nStates + iM_Juv] = rateMatAbLoss + repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iS_Juv] = - (mu1DD_plus_juvMatRate + beta_x_allI) + repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iI_Juv] = repeatedValue[iS_Juv] + pd[iS_Juv * nStates + iI_Pup];
	pd[iS_Juv * nStates + iR_Juv] = rateAbLoss * pR2S + repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iL_Juv] = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iM_Imm] = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iS_Imm] = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iI_Imm] = pd[iS_Juv * nStates + iI_Juv];
	pd[iS_Juv * nStates + iR_Imm] = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iL_Imm] = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iS_Ad]  = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iI_Ad]  = pd[iS_Juv * nStates + iI_Juv];
	pd[iS_Juv * nStates + iR_Ad]  = repeatedValue[iS_Juv];
	pd[iS_Juv * nStates + iL_Ad]  = repeatedValue[iS_Juv];
    /* Partial derivatives - column 8 */
	/* ydot[iI_Juv] = weanRate * I_Pup + S_Juv * beta_x_allI - I_Juv * (mu1DD_plus_juvMatRate + rateRecovery) */
	repeatedValue[iI_Juv]         = - I_Juv * mu2 / K;
    pd[iI_Juv * nStates + iI_Pup] = weanRate + beta * S_Juv;
    pd[iI_Juv * nStates + iM_Imm] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iS_Imm] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iI_Imm] = beta * S_Juv + repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iR_Imm] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iL_Imm] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iM_Juv] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iS_Juv] = beta_x_allI + repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iI_Juv] = pd[iI_Juv * nStates + iI_Imm] - (mu1DD_plus_juvMatRate + rateRecovery);
    pd[iI_Juv * nStates + iR_Juv] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iL_Juv] = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iS_Ad]  = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iI_Ad]  = pd[iI_Juv * nStates + iI_Imm];
    pd[iI_Juv * nStates + iR_Ad]  = repeatedValue[iI_Juv];
    pd[iI_Juv * nStates + iL_Ad]  = repeatedValue[iI_Juv];
    /* Partial derivatives - column 9 */
	/* ydot[iR_Juv] = weanRate * R_Pup + rateRecovery * I_Juv + beta_x_pL2R_x_allI * L_Juv - R_Juv * (mu1DD_plus_juvMatRate + rateAbLoss) */
	repeatedValue[iR_Juv] = - R_Juv * mu2 / K;
	pd[iR_Juv * nStates + iI_Pup] = beta_x_pL2R * L_Juv;
    pd[iR_Juv * nStates + iR_Pup] = weanRate;
	pd[iR_Juv * nStates + iM_Juv] = repeatedValue[iR_Juv];
    pd[iR_Juv * nStates + iS_Juv] = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iI_Juv] = rateRecovery + repeatedValue[iM_Juv] + beta_x_pL2R * L_Juv;
    pd[iR_Juv * nStates + iR_Juv] = -(mu1DD_plus_juvMatRate + rateAbLoss) + repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iL_Juv] = repeatedValue[iM_Juv] + beta_x_pL2R_x_allI;
    pd[iR_Juv * nStates + iM_Imm] = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iS_Imm] = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iI_Imm] = repeatedValue[iM_Juv] + beta_x_pL2R * L_Juv;
    pd[iR_Juv * nStates + iR_Imm] = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iL_Imm] = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iS_Ad]  = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iI_Ad]  = repeatedValue[iM_Juv] + beta_x_pL2R * L_Juv;
    pd[iR_Juv * nStates + iR_Ad]  = repeatedValue[iM_Juv];
    pd[iR_Juv * nStates + iL_Ad]  = repeatedValue[iM_Juv];
	/* Partial derivatives - column 10 */
	/* ydot[iL_Juv] = weanRate * L_Pup + rateAbLoss * R_Juv * pR2L - L_Juv * (mu1DD_plus_juvMatRate + beta_x_pL2R_x_allI) */
	repeatedValue[iL_Juv]         = - L_Juv * mu2 / K;
    pd[iL_Juv * nStates + iI_Pup] = - L_Juv* beta_x_pL2R;
	pd[iL_Juv * nStates + iL_Pup] = weanRate;
	pd[iL_Juv * nStates + iM_Juv] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iS_Juv] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iI_Juv] = repeatedValue[iL_Juv] - L_Juv* beta_x_pL2R;
	pd[iL_Juv * nStates + iR_Juv] = rateAbLoss * pR2L + repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iL_Juv] = - mu1DD_plus_juvMatRate + repeatedValue[iL_Juv] + beta_x_pL2R_x_allI;
	pd[iL_Juv * nStates + iM_Imm] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iS_Imm] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iI_Imm] = repeatedValue[iL_Juv] - L_Juv* beta_x_pL2R;
	pd[iL_Juv * nStates + iR_Imm] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iL_Imm] = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iS_Ad]  = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iI_Ad]  = repeatedValue[iL_Juv] - L_Juv* beta_x_pL2R;
	pd[iL_Juv * nStates + iR_Ad]  = repeatedValue[iL_Juv];
	pd[iL_Juv * nStates + iL_Ad]  = repeatedValue[iL_Juv];
	/* Partial derivatives - column 11 */
    /* ydot[iM_Imm] = juvMatRate * M_Juv - M_Imm * (mu1DD_plus_immMatRate + rateMatAbLoss) */
	repeatedValue[iM_Imm]         = - M_Imm * mu2 / K;
	pd[iM_Imm * nStates + iM_Juv] = juvMatRate + repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iS_Juv] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iI_Juv] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iR_Juv] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iL_Juv] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iM_Imm] = -(mu1DD_plus_immMatRate + rateMatAbLoss) + repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iS_Imm] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iI_Imm] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iR_Imm] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iL_Imm] = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iS_Ad]  = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iI_Ad]  = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iR_Ad]  = repeatedValue[iM_Imm];
	pd[iM_Imm * nStates + iL_Ad]  = repeatedValue[iM_Imm];
	/* Partial derivatives - column 12 */
	/* ydot[iS_Imm] = juvMatRate * S_Juv + rateMatAbLoss * M_Imm + rateAbLoss * R_Imm * pR2S - S_Imm * (mu1DD_plus_immMatRate + beta_x_allI) */
	repeatedValue[iS_Imm]         = - S_Imm * mu2 / K;
	pd[iS_Imm * nStates + iI_Pup] = - S_Imm * beta;
	pd[iS_Imm * nStates + iM_Juv] = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iS_Juv] = juvMatRate + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iI_Juv] = pd[iS_Imm * nStates + iI_Pup] + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iR_Juv] = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iL_Juv] = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iM_Imm] = rateMatAbLoss + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iS_Imm] = -(mu1DD_plus_immMatRate + beta_x_allI) + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iI_Imm] = pd[iS_Imm * nStates + iI_Pup] + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iR_Imm] = rateAbLoss * pR2S + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iL_Imm] = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iS_Ad]  = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iI_Ad]  = pd[iS_Imm * nStates + iI_Pup] + repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iR_Ad]  = repeatedValue[iS_Imm];
	pd[iS_Imm * nStates + iL_Ad]  = repeatedValue[iS_Imm];
	/* Partial derivatives - column 13 */
	/* ydot[iI_Imm] = juvMatRate * I_Juv + S_Imm * beta_x_allI - I_Imm * (mu1DD_plus_immMatRate + rateRecovery) */
	repeatedValue[iI_Imm]         = - I_Imm * mu2 / K;
	pd[iI_Imm * nStates + iI_Pup] = beta * S_Imm;
	pd[iI_Imm * nStates + iM_Juv] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iS_Juv] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iI_Juv] = juvMatRate + pd[iI_Imm * nStates + iI_Pup] + repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iR_Juv] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iL_Juv] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iM_Imm] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iS_Imm] = beta_x_allI + repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iI_Imm] = pd[iI_Imm * nStates + iI_Pup] - (mu1DD_plus_immMatRate + rateRecovery) + repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iR_Imm] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iL_Imm] = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iS_Ad]  = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iI_Ad]  = pd[iI_Imm * nStates + iI_Pup] + repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iR_Ad]  = repeatedValue[iI_Imm];
	pd[iI_Imm * nStates + iL_Ad]  = repeatedValue[iI_Imm];
	/* Partial derivatives - column 14 */
	/* ydot[iR_Imm] = juvMatRate * R_Juv + rateRecovery * I_Imm + beta_x_pL2R_x_allI * L_Imm - R_Imm * (mu1DD_plus_immMatRate + rateAbLoss) */
	repeatedValue[iR_Imm]         = - R_Imm * mu2 / K;
	pd[iR_Imm * nStates + iI_Pup] = beta_x_pL2R * L_Imm;
	pd[iR_Imm * nStates + iM_Juv] = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iS_Juv] = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iI_Juv] = repeatedValue[iR_Imm] + beta_x_pL2R * L_Imm;
	pd[iR_Imm * nStates + iR_Juv] = juvMatRate + repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iL_Juv] = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iM_Imm] = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iS_Imm] = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iI_Imm] = rateRecovery + repeatedValue[iR_Imm] + beta_x_pL2R * L_Imm;
	pd[iR_Imm * nStates + iR_Imm] = -(mu1DD_plus_immMatRate + rateAbLoss) + repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iL_Imm] = repeatedValue[iR_Imm] + beta_x_pL2R_x_allI;
	pd[iR_Imm * nStates + iS_Ad]  = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iI_Ad]  = repeatedValue[iR_Imm] + beta_x_pL2R * L_Imm;
	pd[iR_Imm * nStates + iR_Ad]  = repeatedValue[iR_Imm];
	pd[iR_Imm * nStates + iL_Ad]  = repeatedValue[iR_Imm];
	/* Partial derivatives - column 15 */
	/* ydot[iL_Imm] = juvMatRate * L_Juv + rateAbLoss * R_Imm * pR2L - L_Imm * (mu1DD_plus_immMatRate + beta_x_pL2R_x_allI) */
	repeatedValue[iL_Imm]         = - L_Imm * mu2 / K;
	pd[iL_Imm * nStates + iI_Pup] = - L_Imm * beta_x_pL2R;
	pd[iL_Imm * nStates + iM_Juv] = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iS_Juv] = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iI_Juv] = repeatedValue[iL_Imm] - L_Imm * beta_x_pL2R;
	pd[iL_Imm * nStates + iR_Juv] = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iL_Juv] = juvMatRate + repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iM_Imm] = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iS_Imm] = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iI_Imm] = repeatedValue[iL_Imm] - L_Imm * beta_x_pL2R;
	pd[iL_Imm * nStates + iR_Imm] = rateAbLoss * pR2L + repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iL_Imm] = - (mu1DD_plus_immMatRate) + repeatedValue[iL_Imm] - beta_x_pL2R_x_allI;
	pd[iL_Imm * nStates + iS_Ad]  = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iI_Ad]  = repeatedValue[iL_Imm] - L_Imm * beta_x_pL2R;
	pd[iL_Imm * nStates + iR_Ad]  = repeatedValue[iL_Imm];
	pd[iL_Imm * nStates + iL_Ad]  = repeatedValue[iL_Imm];
	/* Partial derivatives - column 16 */
	/* ydot[iS_Ad]  = immMatRate * (S_Imm + M_Imm) + rateAbLoss * R_Ad * pR2S - S_Ad * (mu2DD + beta_x_allI) */
	repeatedValue[iS_Ad]         = - S_Ad * mu2 / K;
	pd[iS_Ad * nStates + iI_Pup] = - S_Ad * beta;
	pd[iS_Ad * nStates + iM_Juv] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iS_Juv] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iI_Juv] = pd[iS_Ad * nStates + iI_Pup] + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iR_Juv] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iL_Juv] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iM_Imm] = immMatRate + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iS_Imm] = immMatRate + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iI_Imm] = pd[iS_Ad * nStates + iI_Pup] + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iR_Imm] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iL_Imm] = repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iS_Ad]  = -(mu2DD + beta_x_allI) + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iI_Ad]  = pd[iS_Ad * nStates + iI_Pup] + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iR_Ad]  = rateAbLoss * pR2S + repeatedValue[iS_Ad];
	pd[iS_Ad * nStates + iL_Ad]  = repeatedValue[iS_Ad];
	/* Partial derivatives - column 17 */
	/* ydot[iI_Ad]  = immMatRate * I_Imm + S_Ad * beta_x_allI - I_Ad * (mu2DD + rateRecovery) */
	repeatedValue[iI_Ad]         = - I_Ad * mu2 / K;
	pd[iI_Ad * nStates + iI_Pup] = beta * S_Ad;
	pd[iI_Ad * nStates + iM_Juv] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iS_Juv] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iI_Juv] = pd[iI_Ad * nStates + iI_Pup] + repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iR_Juv] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iL_Juv] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iM_Imm] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iS_Imm] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iI_Imm] = immMatRate + pd[iI_Ad * nStates + iI_Pup] + repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iR_Imm] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iL_Imm] = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iS_Ad]  = beta_x_allI + repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iI_Ad]  = pd[iI_Ad * nStates + iI_Pup] - (mu2DD + rateRecovery) + repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iR_Ad]  = repeatedValue[iI_Ad];
	pd[iI_Ad * nStates + iL_Ad]  = repeatedValue[iI_Ad];
	/* Partial derivatives - column 18 */
	/* ydot[iR_Ad]  = immMatRate * R_Imm + rateRecovery * I_Ad + beta_x_pL2R_x_allI * L_Ad - R_Ad * (mu2DD + rateAbLoss) */
	repeatedValue[iR_Ad]         = - R_Ad * mu2 / K;
    pd[iR_Ad * nStates + iI_Pup] = beta_x_pL2R * L_Ad;
	pd[iR_Ad * nStates + iM_Juv] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iS_Juv] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iI_Juv] = repeatedValue[iR_Ad] + beta_x_pL2R * L_Ad;
	pd[iR_Ad * nStates + iR_Juv] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iL_Juv] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iM_Imm] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iS_Imm] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iI_Imm] = repeatedValue[iR_Ad] + beta_x_pL2R * L_Ad;
	pd[iR_Ad * nStates + iR_Imm] = immMatRate + repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iL_Imm] = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iS_Ad]  = repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iI_Ad]  = rateRecovery + repeatedValue[iR_Ad] + beta_x_pL2R * L_Ad;
	pd[iR_Ad * nStates + iR_Ad]  = -(mu2DD + rateAbLoss) + repeatedValue[iR_Ad];
	pd[iR_Ad * nStates + iL_Ad]  = repeatedValue[iR_Ad] + beta_x_pL2R_x_allI;
	/* Partial derivatives - column 19 */
	/* ydot[iL_Ad]  = immMatRate * L_Imm + rateAbLoss * R_Ad * pR2L - L_Ad * (mu2DD + beta_x_pL2R_x_allI) */
	repeatedValue[iL_Ad]         = - L_Ad * mu2 / K;
    pd[iL_Ad * nStates + iI_Pup] = - L_Ad * beta_x_pL2R;
    pd[iL_Ad * nStates + iM_Juv] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iS_Juv] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iI_Juv] = repeatedValue[iL_Ad] - L_Ad * beta_x_pL2R;;
	pd[iL_Ad * nStates + iR_Juv] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iL_Juv] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iM_Imm] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iS_Imm] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iI_Imm] = repeatedValue[iL_Ad] - L_Ad * beta_x_pL2R;;
	pd[iL_Ad * nStates + iR_Imm] = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iL_Imm] = immMatRate + repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iS_Ad]  = repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iI_Ad]  = repeatedValue[iL_Ad] - L_Ad * beta_x_pL2R;;
	pd[iL_Ad * nStates + iR_Ad]  = rateAbLoss * pR2L + repeatedValue[iL_Ad];
	pd[iL_Ad * nStates + iL_Ad]  = repeatedValue[iL_Ad] - (mu2DD + beta_x_pL2R_x_allI);
	/* Partial derivatives - column 20 */
	/* ydot[iSurv]  = -Surv * mu2DD */
	repeatedValue[iSurv]         = - Surv * mu2 / K;
	pd[iSurv * nStates + iM_Juv] = repeatedValue[iSurv];
	pd[iSurv * nStates + iS_Juv] = repeatedValue[iSurv];
	pd[iSurv * nStates + iI_Juv] = repeatedValue[iSurv];
	pd[iSurv * nStates + iR_Juv] = repeatedValue[iSurv];
	pd[iSurv * nStates + iL_Juv] = repeatedValue[iSurv];
	pd[iSurv * nStates + iM_Imm] = repeatedValue[iSurv];
	pd[iSurv * nStates + iS_Imm] = repeatedValue[iSurv];
	pd[iSurv * nStates + iI_Imm] = repeatedValue[iSurv];
	pd[iSurv * nStates + iR_Imm] = repeatedValue[iSurv];
	pd[iSurv * nStates + iL_Imm] = repeatedValue[iSurv];
	pd[iSurv * nStates + iS_Ad]  = repeatedValue[iSurv];
	pd[iSurv * nStates + iI_Ad]  = repeatedValue[iSurv];
	pd[iSurv * nStates + iR_Ad]  = repeatedValue[iSurv];
	pd[iSurv * nStates + iL_Ad]  = repeatedValue[iSurv];
	pd[iSurv * nStates + iSurv]  = - mu2DD;
}


/* END file model.c */
