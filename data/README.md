The DataEbolaCameroun.csv file is used for the serology data. Columns used in our paper include

Date            - date in verbose format
DateMission     - date in day/month/year format
ReproStatus     - a factor with the following levels
                  FemAd         - female adult
                  FemAdGest     - gestating female adult
                  FemAdImmature - female immature adult
                  FemAdLact     - lactating female adult
                  FemJuv        - juvenile female
                  MalAd         - male adult
                  MalImm        - male immature adult
                  MalJuv        - jevenile male
Res1GP.ZEBVkiss - serological results of the serological test used in hte current analysis


The file Ebola_PCR_Eidolon_Yaounde.xlsx is used for the PCR data.
It is a subset of the data pressented in DataEbolaCameroun.csv that includes only the bats tested by PCR.
Columns used in our paper include

Date		- as above
ID			- as above
PCR FILO	- all negative
Site Region - as above
reproStatus - as above
sex			- as above



Other columns in these files that we don't use, include

Age							- categorical age class, with pups, juveniles, immatures and adults
Colour_fur_dorsal			- fur colour on back
Colour_fur_ventral			- fur colour on fron
Consensus					- consensus for full latin name of species
Consensus family			- missing
Consensus genre				- field team consensus about the genre of the individual
Consensus species			- field team consensus about the species of the individual
Country						- which country.
Date experiment				- missing
GPS							- GPS coordinates
GPS_comment					- comments about GPS coordinates
GPS_lat_corrected			- corrected GPS coordinates
GPS_long_corrected			- corrected GPS coordinates
ID							- bat ID
ID Mother					- Identification code of mother, if known
In:							- describes capture locaion as town, plantation or zoo
L_forearm_mm				- forearm length in millimeters
L_metacarpal_3rd_finger_mm	- length of 3rd metacarpal
L_tail_mm					- tail length
L_total_body_mm				- total body length in mm
Lactation					- indicator of lactation
Latitude (N or S)			- northern or southern latitude
Longitude (E or W)			- eastern or western longitude
Method of capture			- factor with level of nets
Mission						- The month of the field study (mission)
Place of Capture			- describes capture locaion as village, town or other
Pregnant					- indicator of pregnancy
Progeny						- indicator of carrying pups
Proximity to Village		- how close the capture was to a village.
Sex							- male or female
Site Precise				- site location at a local level
Site Region					- site location at "regional" level
Size_eyes					- desription of eye size
Team						- which team did the work
Weight_gr					- weight in grams


A series of columns follow refering to various other serology tests performed in the Djomsi et al 2022 study - see that paper for details.
Those columns, which were not used in our mdelling paper, include
Res1NP-ZEBV
Res2NP-ZEBV
RecNP-ZEBV
Res1GP-ZEBVkiss
Res2GP-ZEBVkiss
RecGP-ZEBVkiss
RecGP-ZEBVmay
RecVP40-ZEBV
RecNP-SEBV
RecGP-SEBV
RecVP40-SEBV
RecGP-BEBV
RecVP40-BEBV
RecGP-REBV
RecGP-BOMV
Date experiment
RecNP-ZEBV
RecGP-ZEBVkiss
RecGP-ZEBVmay
RecVP40-ZEBV
RecNP-SEBV
RecGP-SEBV
RecVP40-SEBV
RecGP-BEBV
RecVP40-BEBV
RecGP-REBV
RecGP-BOMV
Date experiment
RecNP-ZEBV
RecGP-ZEBVkiss
RecGP-ZEBVmay
RecVP40-ZEBV
RecNP-SEBV
RecGP-SEBV
RecVP40-SEBV
RecGP-BEBV
RecVP40-BEBV
RecGP-REBV
RecGP-BOMV
article 2018
sequence_cytB
sequence_12S
Results RT-qPCR (NIH-pan Filo)
PCR NP Zaire
seq NP-Zaire
PCR NP sudan
seq NP-sudan
PCR VP35 Zaire
seq VP35 Zaire
