# Code for the paper **A Bayesian analysis of birth pulse effectson the probability of detecting Ebola virus in fruit bats**

This repository includes all code and data used in the analysis of the Yaounde-bat-Ebola modelling paper.

Key scripts in the **R** directory include
1. modelDefinition_BatMSIRL.R - reads and filters data and builds nimble model
1. functions.R - a list of functions used in the above scripts
2. runMcmc_BatSIR.R - uses nimble model to perform MCMC
3. inference.R - generates plots and tables from MCMC output
4. longInference.R - generates long trajectories for each line of MCMC output and performs recurrence plot analysis
6. hayman.R - a script for obtaining certain priors from previously published data

Other R scripts include
1. currentFit.R - plots (which may or may not be useful) for model behaviour given the current set of parameters inside the nimble model.
2. priors.R - some plot functions used to visualise priors during the development of the project. May not be up to date with the final set of priors used.

Key data files include
1. data/DataEbolaCameroun.csv - data file used for serology data.
2. data/Ebola_PCR_Eidolon_Yaounde.xlsx - data file used for PCR data.
3. data/doi_10.5061_dryad.2fp34__v1/Eidolon data 2007_2014_Openaccess.csv - unmodified copy of the Peel et al tooth data.


[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.8276172.svg)](https://doi.org/10.5281/zenodo.8276172)
