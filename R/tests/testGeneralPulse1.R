## Test R and C pulse functions give same thing
times = seq(-52*3, 52*3, by=1)
rrrr = 33.33
rC = sapply(times, function(tt) .C("pulseWrapper", x=as.double(tt), tStart=as.double(10), tStop=as.double(30), rateMax=as.double(rrrr), out=as.double(-123))$out)
rR = pulseFunction(x=times, tStart=10, tDuration=20, rate=rrrr)
par(mfrow=n2mfrow(1), lwd=2)
plot(rR ~ times, typ="l", col="blue", ylab="rate")
lines(rC ~ times, typ="l", col="red", lty=2)
