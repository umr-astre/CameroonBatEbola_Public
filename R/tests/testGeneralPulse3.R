if (TRUE) { # FALSE
  .C("initR", parameters = parametersODE) ## R's ODE solver will do something like this. Provides/updates the parameters for C.
  #
  print("#########################################################")
  print("### Test 1: derivatives in C, R at t=1 and difference ###")
  print("#########################################################")
  print("### C ###")
  yC = .C("derivs", neq = as.integer(20), t = as.double(1.0), y = initsODE,
          ydot = as.double(rep(0, 20)), yout = as.double(rep(0, 20)), ip = as.integer(rep(1, 3)))$ydot
  print("### R ###")
  yR = derivsR(time=1, state=initsODE, parameters=parametersODE)[[1]]
  print("### Comparison ###")
  print(cbind(yC, yR, yC-yR))
  ##
  ## print("#############################################################################")
  ## print("### Test 2: min and max ratio of trajectories in C & R: Without Jacobian. ###")
  ## print("#############################################################################")
  ## Time = seq(0, 1*52, by=1)
  ## (tR = system.time(for (i in 1)
  ##   xR = lsoda(y = initsODE, time = Time, parms = parametersODE, func = derivsR)))
  ## (tC = system.time(for (i in 1)
  ##   xC = lsoda(y = initsODE, time = Time, parms = parametersODE, func = "derivs", dllname="model", nout=1)))
  ## nimPrint("max abs difference at t=", tail(Time,1),": ",  max(abs(tail(xR,1)[-1] - tail(xC,1)[-1][1:20])))
  ## print(min(tR/tC, na.rm=T)) # 140 - 166 x faster
  ## print(max(tR/tC, na.rm=T)) # 140 - 166 x faster
  ## print("##########################################################################")
  ## print("### Test 3: min and max ratio of trajectories in C & R: With Jacobian. ###")
  ## print("##########################################################################")
  ## jc =  .C("jac", neq = as.integer(20), t = as.double(1.0), y = initsODE,
  ##          ml = 19, mu = 19, pd = as.numeric(matrix(0.0, 20, 20)), nrowpd = as.integer(20), yout = as.double(rep(0, 20)), ip = as.integer(rep(1, 3)))
  ## max(matrix(jc$pd, 20, 20, TRUE) - jacobianR(time=1, state=initsODE, parameters=parametersODE))
  ## (tR = system.time(for (i in 1)
  ##   xR = lsoda(y = initsODE, time = Time, parms = parametersODE, func = derivsR, jacfunc = jacobianR)))
  ## (tC = system.time(for (i in 1)
  ##   xC = lsoda(y = initsODE, time = Time, parms = parametersODE, func = "derivs", jacfunc = "jac", dllname="model", nout=1)))
  ## nimPrint("max abs difference at t=", tail(Time,1),": ",  max(abs(tail(xR,1)[-1] - tail(xC,1)[-1][1:20])))
  ## print(min(tR/tC, na.rm=T)) # 138 - 159 x faster
  ## print(max(tR/tC, na.rm=T)) # 138 - 159 x faster
  ## # .C("test", a = as.double(5), b = as.double(-6), out=as.double(-99))
}
